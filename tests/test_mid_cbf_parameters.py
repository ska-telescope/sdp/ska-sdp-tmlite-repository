"""
Check that Mid.CBF parameters conform to
schemas
"""

import json

import pytest
from ska_telmodel.schema import validate as telmodel_validate


@pytest.mark.parametrize(
    "parameter_file",
    [
        "tmdata/instrument/ska1_mid_psi/ska-mid-cbf-system-parameters.json",
        "tmdata/instrument/ska1_mid_itf/ska-mid-cbf-system-parameters.json",
    ],
)
def test_mid_cbf_init_sys_parameters(parameter_file):
    """Test input parameter_file conforms to schema"""
    try:
        with open(parameter_file) as f:
            sys_param_json = json.loads(f.read())
            telmodel_validate(
                version=sys_param_json["interface"],
                config=sys_param_json,
                strictness=2,
            )
    except ValueError as e:
        msg = (
            f"Mid.Cbf parameter file {parameter_file} failed validation\n"
            f"with exception:\n {str(e)}"
        )
        assert False, msg
