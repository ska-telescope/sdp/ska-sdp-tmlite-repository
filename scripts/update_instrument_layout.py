
import sys

from ska_telmodel_data.instrument_layout import *

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(
            "Usage: python update_instrument_layout.py [path_to_kml | path_to_dat | path_to_txt ] [path_to_json_or_yaml]"
        )
        exit(1)
        
    apos_by_name = read_layout(sys.argv[1])
    update_receptors_in_file(sys.argv[2], apos_by_name)
