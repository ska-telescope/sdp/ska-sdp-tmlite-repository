

SKA Mid layout
--------------

These files represent the current best understanding for the position
and properties of Meerkat and SKA Mid dishes. current version is
based on SKA-TEL-INSA-0000537 revision 09 from 2021-09-07.

File schemas are documented in telescope model documentation:

https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/ska-telmodel-layout.html
